# Tile scraper:
Program downloads map tiles from for given z, y, z range using different strategies - sequential and concurrent.  
Tiles are saved in the DB as well as data about download process - method, size, time and number of downloaded tiles.  
Tiles are downloaded from Open Street Map service.
[Read more](https://wiki.openstreetmap.org/wiki/Tile_servers)

## How to run

### Install dependencies
```
poetry install
```

### Launch database
```
docker-compose up -d
```

### Run migrations
```
alembic upgrade head
```

### Launch program
Display help
```
python src/scraper.py --help
```

Run specific method
```
python src/scraper.py download-tiles --method asynchronous 1 5 1 5 3 8
```

Run all methods
```
python src/scraper.py download-tiles --method all 1 5 1 5 3 8
```