import click

from typing import Tuple

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from tile_scraper.adapters.database.postgres import SqlMeasurementRepo, SqlTileRepo
from tile_scraper.domain.download.asynchronous import asynchronous
from tile_scraper.domain.download.asynchronous_tasks import asynchronous_tasks
from tile_scraper.domain.download.download import download
from tile_scraper.domain.download.multiprocessing import multiprocessed
from tile_scraper.domain.download.sequential import sequential, sequential_no_session
from tile_scraper.domain.download.thread_pool_executor import thread_pool_executor
from tile_scraper.domain.download.threaded import threaded

DB_URL = 'postgresql://postgres:example@localhost/tiles'
TILE_URL_TEMPLATE = 'https://tile.openstreetmap.org/{z}/{x}/{y}.png'


engine = create_engine(DB_URL)
Session = sessionmaker(bind=engine)
session = Session()

measurement_repo = SqlMeasurementRepo(session=session)
tile_repo = SqlTileRepo(session=session)

methods = ['sequential', 'sequential_no_session', 'asynchronous', 'asynchronous_tasks', 'thread_pool_executor', 'threaded', 'multiprocessed', 'all']


@click.group()
def cli():
    pass

@cli.command()
@click.argument('range_x', nargs=2, type=click.INT)
@click.argument('range_y', nargs=2, type=click.INT)
@click.argument('range_z', nargs=2, type=click.INT)
@click.option('--method', 'method', type=click.Choice(methods, case_sensitive=False))
def download_tiles(range_x: Tuple[int, int], range_y: Tuple[int, int], range_z: Tuple[int, int], method: str):
    if method == 'sequential':
        download(sequential, range_x, range_y, range_z, TILE_URL_TEMPLATE, tile_repo, measurement_repo)
    elif method == 'sequential_no_session':
        download(sequential_no_session, range_x, range_y, range_z, TILE_URL_TEMPLATE, tile_repo, measurement_repo)
    elif method == 'asynchronous':
        download(asynchronous, range_x, range_y, range_z, TILE_URL_TEMPLATE, tile_repo, measurement_repo)
    elif method == 'asynchronous_tasks':
        download(asynchronous_tasks, range_x, range_y, range_z, TILE_URL_TEMPLATE, tile_repo, measurement_repo)
    elif method == 'thread_pool_executor':
        download(thread_pool_executor, range_x, range_y, range_z, TILE_URL_TEMPLATE, tile_repo, measurement_repo)
    elif method == 'threaded':
        download(threaded, range_x, range_y, range_z, TILE_URL_TEMPLATE, tile_repo, measurement_repo)
    elif method == 'multiprocessed':
        download(multiprocessed, range_x, range_y, range_z, TILE_URL_TEMPLATE, tile_repo, measurement_repo)
    elif method == 'all':
        download(sequential, range_x, range_y, range_z, TILE_URL_TEMPLATE, tile_repo, measurement_repo)
        download(sequential_no_session, range_x, range_y, range_z, TILE_URL_TEMPLATE, tile_repo, measurement_repo)
        download(asynchronous, range_x, range_y, range_z, TILE_URL_TEMPLATE, tile_repo, measurement_repo)
        download(asynchronous_tasks, range_x, range_y, range_z, TILE_URL_TEMPLATE, tile_repo, measurement_repo)
        download(thread_pool_executor, range_x, range_y, range_z, TILE_URL_TEMPLATE, tile_repo, measurement_repo)
        download(threaded, range_x, range_y, range_z, TILE_URL_TEMPLATE, tile_repo, measurement_repo)
        download(multiprocessed, range_x, range_y, range_z, TILE_URL_TEMPLATE, tile_repo, measurement_repo)


@cli.command()
def list_tiles():
    click.echo(tile_repo.list())


@cli.command()
def delete_all_tiles():
    tile_repo.delete_all()
    click.echo('All tiles deleted from DB')


@cli.command()
def list_metrics():
    click.echo(measurement_repo.list())


@cli.command()
def delete_all_metrics():
    measurement_repo.delete_all()
    click.echo('All measurements deleted from DB')


@cli.command()
def purge():
    tile_repo.delete_all()
    measurement_repo.delete_all()
    click.echo('DB purged')


if __name__ == '__main__':
    cli()
