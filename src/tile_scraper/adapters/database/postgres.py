from sqlalchemy import Column, Integer, LargeBinary, Float
from sqlalchemy.orm import declarative_base
from sqlalchemy.orm.session import Session
from typing import List

from sqlalchemy.sql.sqltypes import String

from tile_scraper.domain.measurement import Measurement
from tile_scraper.domain.tile import Tile
from tile_scraper.ports.measurement_repo import MeasurementRepoPort
from tile_scraper.ports.tile_repo import TileRepoPort

Base = declarative_base()

class TileModel(Base):
    __tablename__ = 'tile'

    id = Column(Integer, primary_key=True)
    x = Column(Integer)
    y = Column(Integer)
    z = Column(Integer)
    data = Column(LargeBinary)

    def __repr__(self):
        return f'<Tile(x={self.x}, y={self.y}, z={self.z}, size_bytes={len(self.data)})>'


class SqlTileRepo(TileRepoPort):
    def __init__(self, session: Session):
        self.session = session

    def save(self, tile: Tile):

        tile = TileModel(x=tile.x, y=tile.y, z=tile.z, data=tile.data)

        self.session.add(tile)
        self.session.commit()

    def list(self) -> List[Tile]:
        tiles = []

        query_result = self.session.query(TileModel).all()

        for tile in query_result:
            tiles.append(Tile(x=tile.x, y=tile.y, z=tile.z, data=tile.data))
        return tiles

    def delete_all(self):
        self.session.query(TileModel).delete()
        self.session.commit()


class MeasurementModel(Base):
    __tablename__ = 'measurement'

    id = Column(Integer, primary_key=True)
    method = Column(String)
    number_of_tiles = Column(Integer)
    size = Column(Integer)
    execution_time = Column(Float)

class SqlMeasurementRepo(MeasurementRepoPort):
    def __init__(self, session: Session):
        self.session = session

    def save(self, measurement: Measurement) -> None:

        measurement = MeasurementModel(method=measurement.method,
                                       number_of_tiles=measurement.number_of_tiles,
                                       size=measurement.size,
                                       execution_time=measurement.execution_time)

        self.session.add(measurement)
        self.session.commit()

    def list(self) -> List[Measurement]:
        measurements = []

        query_result = self.session.query(MeasurementModel).all()

        for measurement in query_result:
            measurements.append(Measurement(method=measurement.method,
                                            number_of_tiles=measurement.number_of_tiles,
                                            size=measurement.size,
                                            execution_time=measurement.execution_time))
        return measurements

    def delete_all(self):
        self.session.query(MeasurementModel).delete()
        self.session.commit()

