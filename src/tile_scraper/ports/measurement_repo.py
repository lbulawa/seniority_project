from abc import ABC, abstractmethod
from typing import List

from tile_scraper.domain.measurement import Measurement

class MeasurementRepoPort(ABC):
    @abstractmethod
    def save(self, tile: Measurement) -> None:
        pass

    @abstractmethod
    def list(self) -> List[Measurement]:
        pass
