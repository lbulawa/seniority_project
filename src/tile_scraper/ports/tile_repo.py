from abc import ABC, abstractmethod
from typing import List

from tile_scraper.domain.tile import Tile

class TileRepoPort(ABC):
    @abstractmethod
    def save(self, tile: Tile) -> None:
        pass

    @abstractmethod
    def list(self) -> List[Tile]:
        pass
