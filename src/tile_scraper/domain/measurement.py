from dataclasses import dataclass

@dataclass
class Measurement:
    method: str
    number_of_tiles: int
    size: int
    execution_time: float

    def __repr__(self):
        return f'<Measurement(method={self.method}, number_of_tiles={self.number_of_tiles}, size={self.size}, execution_time={self.execution_time}>'
