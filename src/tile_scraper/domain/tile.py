from dataclasses import dataclass

@dataclass
class Tile:
    x: int
    y: int
    z: int
    data: bytes

    def __repr__(self):
        return f'<Tile(x={self.x}, y={self.y}, z={self.z}, size_bytes={len(self.data)}>'
