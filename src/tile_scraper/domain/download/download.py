from itertools import product
from time import time

from typing import Callable, List, Tuple

from tile_scraper.domain.measurement import Measurement
from tile_scraper.ports.measurement_repo import MeasurementRepoPort
from tile_scraper.ports.tile_repo import TileRepoPort


def _expand_ranges(range_x: Tuple[int, int], range_y: Tuple[int, int], range_z: Tuple[int, int]) -> List:
    return list(product(range(range_x[0], range_x[1] + 1), range(range_y[0], range_y[1] + 1), range(range_z[0], range_z[1] + 1)))


def download(download_func: Callable, range_x: Tuple[int, int], range_y: Tuple[int, int], range_z: Tuple[int, int],
             url_template: str, tile_repo: TileRepoPort, measurement_repo: MeasurementRepoPort):
    
    ranges = _expand_ranges(range_x, range_y, range_z)
    number_of_tiles = len(ranges)

    print(f'Downloading tiles from range {range_x} {range_y} {range_z} using {download_func.__name__} method')

    start_time = time()

    size = download_func(ranges, url_template, tile_repo)

    execution_time = time() - start_time

    print('Saved')

    measurement = Measurement(method=download_func.__name__, number_of_tiles=number_of_tiles, size=size, execution_time=execution_time)
    measurement_repo.save(measurement)

    print(f'Size: {size}')
    print(f'Time: {execution_time} seconds')
    print(f'Number of tiles: {number_of_tiles}')
