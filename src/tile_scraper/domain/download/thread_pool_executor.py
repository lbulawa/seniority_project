from concurrent.futures import ThreadPoolExecutor, as_completed
from typing import List, Tuple

import requests

from tile_scraper.domain.tile import Tile
from tile_scraper.ports.tile_repo import TileRepoPort


def _download(url_template: str, range_xyz: Tuple) -> Tile:

    x, y, z = range_xyz

    resp = requests.get(url_template.format(z=z, x=x, y=y))

    tile_content = resp.content
    tile = Tile(x, y, z, tile_content)

    return tile


def thread_pool_executor(ranges: List[Tuple[int, int, int]], url_template: str, tile_repo: TileRepoPort) -> int:

    size = 0
    processes = []

    with ThreadPoolExecutor() as executor:
        for range_xyz in ranges:
            processes.append(executor.submit(_download, url_template, range_xyz))

    for task in as_completed(processes):
        tile = task.result()
        size = size + len(tile.data)

        tile_repo.save(tile)

    return size
