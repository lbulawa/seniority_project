import multiprocessing
import requests

from typing import List, Tuple

from tile_scraper.domain.tile import Tile
from tile_scraper.ports.tile_repo import TileRepoPort


TILE_URL_TEMPLATE = 'https://tile.openstreetmap.org/{z}/{x}/{y}.png'


session = None
que = multiprocessing.Queue()


def _set_global_session():
    global session
    if not session:
        session = requests.Session()


def _download_tile(range_xyz: Tuple):
    _set_global_session()

    x, y, z = range_xyz

    with session.get(TILE_URL_TEMPLATE.format(z=z, x=x, y=y)) as resp:
        tile_content = resp.content
        tile = Tile(x, y, z, tile_content)

        que.put(tile)


def _download_all_tiles(ranges: List[Tuple[int, int, int]], url_template: str):

    with multiprocessing.Pool(initializer=_set_global_session) as pool:
        pool.map(_download_tile, ranges)

    tiles = []

    while not que.empty():
        tile = que.get()
        tiles.append(tile)

    return tiles


def multiprocessed(ranges: List[Tuple[int, int, int]], url_template: str, tile_repo: TileRepoPort) -> int:

    size = 0

    tiles = _download_all_tiles(ranges, url_template)

    for tile in tiles:
        size = size + len(tile.data)
        tile_repo.save(tile)

    return size
