import requests
import threading
import queue

from typing import List, Tuple

from tile_scraper.domain.tile import Tile
from tile_scraper.ports.tile_repo import TileRepoPort


thread_local = threading.local()
que = queue.Queue()


def _get_session():
    if not hasattr(thread_local, 'session'):
        thread_local.session = requests.Session()
    return thread_local.session


def _download_tile(range_xyz: Tuple, url_template: str):

    x, y, z = range_xyz

    session = _get_session()

    with session.get(url_template.format(z=z, x=x, y=y)) as resp:
        tile_content = resp.content
        tile = Tile(x, y, z, tile_content)

        que.put(tile)


def _download_all_tiles(ranges: List[Tuple[int, int, int]], url_template: str) -> List[Tile]:

    threads = []

    for range_xyz in ranges:
        thread = threading.Thread(target=_download_tile, args=(range_xyz, url_template))
        threads.append(thread)
        thread.start()

    for thread in threads:
        thread.join()

    tiles = []

    while not que.empty():
        tile = que.get()
        tiles.append(tile)

    return tiles


def threaded(ranges: List[Tuple[int, int, int]], url_template: str, tile_repo: TileRepoPort) -> int:

    size = 0

    tiles = _download_all_tiles(ranges, url_template)

    for tile in tiles:
        size = size + len(tile.data)
        tile_repo.save(tile)

    return size
