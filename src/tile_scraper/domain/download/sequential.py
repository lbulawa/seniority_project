import requests

from typing import List, Tuple

from tile_scraper.domain.tile import Tile
from tile_scraper.ports.tile_repo import TileRepoPort


def sequential(ranges: List[Tuple[int, int, int]], url_template: str, tile_repo: TileRepoPort) -> int:

    size = 0
    tiles = []

    with requests.Session() as session:
        for x, y, z in ranges:
            with session.get(url_template.format(z=z, x=x, y=y)) as resp:
                tile_content = resp.content
                tile = Tile(x, y, z, tile_content)
                tiles.append(tile)

    for tile in tiles:
        tile_repo.save(tile)
        size = size + len(tile.data)

    return size


def sequential_no_session(ranges: List[Tuple[int, int, int]], url_template: str, tile_repo: TileRepoPort) -> int:

    size = 0
    tiles = []

    for x, y, z in ranges:
        resp = requests.get(url_template.format(z=z, x=x, y=y))
        tile_content = resp.content
        tile = Tile(x, y, z, tile_content)
        tiles.append(tile)

    for tile in tiles:
        tile_repo.save(tile)
        size = size + len(tile.data)

    return size
