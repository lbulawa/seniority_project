import asyncio

from aiohttp import ClientSession
from typing import List, Tuple

from tile_scraper.domain.tile import Tile
from tile_scraper.ports.tile_repo import TileRepoPort


HEADERS = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)"} # looks like aiohttp has weird user-agent which is rejected by openstreetmap


async def _download(ranges: List[Tuple[int, int, int]], url_template: str) -> int:

    tiles = []

    async with ClientSession(headers=HEADERS) as session:
        for x, y, z in ranges:
            async with session.get(url_template.format(z=z, x=x, y=y)) as resp:
                tile_content = await resp.content.read()
                tile = Tile(x, y, z, tile_content)
                tiles.append(tile)

    return tiles


def asynchronous(ranges: List[Tuple[int, int, int]], url_template: str, tile_repo: TileRepoPort) -> int:

    size = 0

    results = asyncio.run(_download(ranges, url_template))

    for tile in results:
        size = size + len(tile.data)
        tile_repo.save(tile)

    return size
