"""create_measurement_model

Revision ID: 6b46caca370f
Revises: a7605bfd825e
Create Date: 2021-11-24 09:31:55.888214

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6b46caca370f'
down_revision = 'a7605bfd825e'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'measurement',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('method', sa.String),
        sa.Column('number_of_tiles', sa.Integer),
        sa.Column('size', sa.Integer),
        sa.Column('execution_time', sa.Float)
    )


def downgrade():
    op.drop_table('measurement')
