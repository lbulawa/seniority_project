"""create_tile_model

Revision ID: a7605bfd825e
Revises: e41f8a3530f5
Create Date: 2021-11-09 10:53:37.261567

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a7605bfd825e'
down_revision = 'e41f8a3530f5'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'tile',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('x', sa.Integer, nullable=False),
        sa.Column('y', sa.Integer, nullable=False),
        sa.Column('z', sa.Integer, nullable=False),
        sa.Column('data', sa.LargeBinary, nullable=False),
    )


def downgrade():
    op.drop_table('tile')
